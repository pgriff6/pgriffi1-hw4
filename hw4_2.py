from mininet.net import Mininet
from mininet.node import Controller, RemoteController, Node
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import Link, Intf
from mininet.link import TCLink

def aggNet():

	CONTROLLER_IP='127.0.0.1' # Assign IP to your controller

	net = Mininet(topo=None,build=False,link=TCLink)

	# Instruction to add Controller
	c0 = net.addController('c0',controller=RemoteController,ip=CONTROLLER_IP,port=6633)
	# Add hosts and switch according to topology specified
	h1 = net.addHost('h1',ip='0.0.0.0')
	h2 = net.addHost('h2',ip='0.0.0.0')
	h3 = net.addHost('h3',ip='0.0.0.0')
	h4 = net.addHost('h4',ip='0.0.0.0')
	h5 = net.addHost('h5',ip='0.0.0.0')
	h6 = net.addHost('h6',ip='0.0.0.0')
	s1 = net.addSwitch('s1')
	s2 = net.addSwitch('s2')
	s3 = net.addSwitch('s3')
	# Add Links
	net.addLink(h1,s1)
	net.addLink(h2,s1)
	net.addLink(h3,s2,delay='400ms')
	net.addLink(h4,s3)
	net.addLink(h5,s3)
	net.addLink(h6,s3)
	net.addLink(s1,s2)
	net.addLink(s2,s3)

	net.start()

	# Run DHCP
	h1.cmd('dhclient h1-eth0')
	h2.cmd('dhclient h2-eth0')
	h3.cmd('dhclient h3-eth0')
	h4.cmd('dhclient h4-eth0')
	h5.cmd('dhclient h5-eth0')
	h6.cmd('dhclient h6-eth0')
	# Launch webservers on h4 and h5
	h4.cmd('python simplehttpwebpage.py good.html &')
	h5.cmd('python simplehttpwebpage.py evil.html &')
	# Launch the DNS resolver on h3
	h3.cmd('python SimpleDNSResolver.py 10.2.1.1 h3-eth0 dns_h3.conf &')
	# Launch dnsmasq on h2
	h2.cmd('dnsmasq &')

	CLI(net)
	net.stop()

if __name__ == '__main__':
	setLogLevel('info')
	aggNet()
